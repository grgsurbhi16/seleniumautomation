package com.hcl.auto.TestNgClasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.hcl.auto.Utilities.*;

import com.hcl.auto.Utilities.BrowserUtils;

public class TestNGClass1 {
	@Parameters("Browser")
	
	@Test
	public void c1m1(){
		//WebDriver d = new ChromeDriver();
		//d.get("http://google.com");
		System.out.println("c1m1");
		WebDriver d=BrowserUtils.invokeBrowser("Browser");
		d.get("https://www.google.com");
		}
	@Test
	void c1m2(){
		System.out.println("c1m2");
	
	}
	@Test
	void c1m3(){
		System.out.println("c1m3");
	
	}
	

}