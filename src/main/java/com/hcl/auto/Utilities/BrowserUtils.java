package com.hcl.auto.Utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserUtils {

	static WebDriver d = null;

	public static WebDriver invokeBrowser(String browser) {
		switch (browser) {
		case "ch":
			d = new ChromeDriver();
			break;
		case "ff":
			d = new FirefoxDriver();
			break;
		default:
			d = new ChromeDriver();
			break;
		}

		return d;

	}

}
